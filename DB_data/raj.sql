-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2017 at 02:30 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raj`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text NOT NULL,
  `rule_name` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `created_date` date NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `id` int(11) NOT NULL,
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `data` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `multilingual_data`
--

CREATE TABLE `multilingual_data` (
  `id` int(11) NOT NULL,
  `record_type` varchar(255) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_value` text NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oreders`
--

CREATE TABLE `oreders` (
  `id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `pincode` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `comments` text NOT NULL,
  `payment_by` tinyint(1) NOT NULL,
  `booking_status` tinyint(1) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `paid_amount` float NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `delivery_charges` decimal(10,2) NOT NULL,
  `tax_price` decimal(10,2) NOT NULL,
  `order_number` varchar(23) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_status` varchar(50) NOT NULL,
  `cancellation_reason` text NOT NULL,
  `cancellation_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oreders`
--

INSERT INTO `oreders` (`id`, `buyer_id`, `name`, `address`, `location_name`, `city_name`, `pincode`, `email`, `mobile`, `comments`, `payment_by`, `booking_status`, `total_price`, `paid_amount`, `subtotal`, `delivery_charges`, `tax_price`, `order_number`, `transaction_id`, `payment_date`, `created_at`, `payment_status`, `cancellation_reason`, `cancellation_date`) VALUES
(1, 2, 'Seeta', 'c-53 Malviya Nagar jaipur, Rajastha 301001', 'jaipur', 'jaipur', 301001, 'seeta@gmail.com', '123456', 'Nice dealer', 1, 1, '2000.00', 2000, '2000.00', '0.00', '20.57', '1', '1235456', '2017-03-20 00:00:00', '2017-03-20 17:03:49', 'DONE', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oreders_details`
--

CREATE TABLE `oreders_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `size_price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oreders_details`
--

INSERT INTO `oreders_details` (`id`, `order_id`, `item_name`, `size_price`, `quantity`, `created_at`, `item_id`) VALUES
(1, 1, 'Pot', '20.00', 10, '2017-03-20 17:06:53', 2);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_category` int(11) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `is_available` int(1) NOT NULL,
  `product_owner` int(11) NOT NULL,
  `price` float NOT NULL,
  `stock` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_category`, `product_image`, `is_available`, `product_owner`, `price`, `stock`, `created_date`, `modified_date`) VALUES
(1, 'Kushan', 1, 'kushan.png', 1, 1, 20, '500', '2017-03-20', '2017-03-20 16:39:45'),
(2, 'Diya', 2, 'deep.png', 1, 2, 5, '10000', '2017-03-18', '2017-03-20 16:54:42'),
(3, 'Pot', 2, 'pot.png', 1, 1, 20, '50', '2017-03-19', '2017-03-20 16:54:42');

-- --------------------------------------------------------

--
-- Table structure for table `product_catgory`
--

CREATE TABLE `product_catgory` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_date` date NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_catgory`
--

INSERT INTO `product_catgory` (`id`, `category_name`, `created_date`, `modified_date`) VALUES
(1, 'Handicrafted', '2017-03-20', '2017-03-20 16:37:55'),
(2, 'Pots', '2017-03-19', '2017-03-20 16:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` date NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_logged_in` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `full_name`, `email`, `password_hash`, `auth_key`, `password_reset_token`, `is_active`, `created_date`, `modified_date`, `last_logged_in`) VALUES
(1, 'renumeena', 'Renu Meena', 'xyz@gmail.com', '', '', '', 1, '2017-03-20', '2017-03-20 14:40:52', '2017-03-19 07:00:00'),
(2, 'seeta', 'seeta rathore', 'seeta@seeta.com', '', '', '', 1, '2017-03-20', '2017-03-20 17:27:07', '2017-03-18 11:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_familly_info`
--

CREATE TABLE `user_familly_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_selling` tinyint(1) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `map_address` text NOT NULL,
  `dd_antyodya_id` varchar(255) DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `user_id`, `is_selling`, `latitude`, `longitude`, `map_address`, `dd_antyodya_id`, `modified_date`) VALUES
(1, 1, 1, 26.8505, 75.8002, 'malviya nagar jaipur, 302020', '5f4dcc3b5aa765d61d8327deb882cf99', '2017-03-20 22:35:08'),
(2, 2, 0, 75.8002, 12.452, 'malviya nagar', '1a1dc91c907325c69271ddf0c944bc72', '2017-03-20 22:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_languages`
--

CREATE TABLE `user_languages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `is_read` tinyint(1) NOT NULL,
  `is_speak` tinyint(1) NOT NULL,
  `is_write` tinyint(1) NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `dob` date NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `marital_status` enum('married','single','divorced') NOT NULL,
  `address` text NOT NULL,
  `education_background` varchar(255) NOT NULL,
  `nick_name` varchar(255) NOT NULL,
  `nationality` int(11) NOT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`id`, `user_id`, `gender`, `dob`, `is_verified`, `marital_status`, `address`, `education_background`, `nick_name`, `nationality`, `modified_date`) VALUES
(1, 1, 'Male', '1991-02-09', 1, 'married', 'g45 malviya nagar jaipur', 'B.Tech', 'renu', 1, '2017-03-20 17:25:16');

-- --------------------------------------------------------

--
-- Table structure for table `yiisession`
--

CREATE TABLE `yiisession` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `yiisession`
--

INSERT INTO `yiisession` (`id`, `expire`, `data`) VALUES
('rcgamr96d3m23s51kt1gdl7us1', 1490063787, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multilingual_data`
--
ALTER TABLE `multilingual_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oreders`
--
ALTER TABLE `oreders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oreders_details`
--
ALTER TABLE `oreders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_catgory`
--
ALTER TABLE `product_catgory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_familly_info`
--
ALTER TABLE `user_familly_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_languages`
--
ALTER TABLE `user_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yiisession`
--
ALTER TABLE `yiisession`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_item`
--
ALTER TABLE `auth_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_rule`
--
ALTER TABLE `auth_rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `multilingual_data`
--
ALTER TABLE `multilingual_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oreders`
--
ALTER TABLE `oreders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oreders_details`
--
ALTER TABLE `oreders_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_catgory`
--
ALTER TABLE `product_catgory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_familly_info`
--
ALTER TABLE `user_familly_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_languages`
--
ALTER TABLE `user_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
