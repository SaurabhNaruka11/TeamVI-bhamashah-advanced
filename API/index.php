<?php

ini_set('upload_max_filesize', '300M');
ini_set('memory_limit', '300M');
ini_set('max_execution_time', '300M');
ini_set('post_max_size', '300M');

// change the following paths if necessary
//$yii=dirname(__FILE__).'/framework/yii.php';
$yii=dirname(__FILE__).'/framework/yiilite.php'; //Added for performance
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',TRUE);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
error_reporting(E_ALL);

ini_set('display_errors', '1');
require_once($yii);
Yii::createWebApplication($config)->run();
