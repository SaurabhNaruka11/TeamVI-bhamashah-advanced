<?php
class CsvFunctions
{

/**
     * Parse a file containing CSV formatted data.
     *
     * @access    public
     * @param    string
     * @param    boolean
     * @return    array
     */
    function parse_file($p_Filepath) {
$separator = ';';
$enclosure = '"'; 
$max_row_size = 4096;
        $file = fopen($p_Filepath, 'r');
        $fields = fgetcsv($file, $max_row_size, $separator, $enclosure);
        $keys_values = explode(',',$fields[0]);

        $content    =   array();
        $keys =   array();
        foreach($keys_values as $row){
            $keys[]   =   str_replace('"', '',$row);
        }
        $i  =   1;
        while( ($row = fgetcsv($file, $max_row_size, $separator, $enclosure)) != false ) {            
            if( $row != null ) { // skip empty lines
                $values =   explode(',',$row[0]);
                if(count($keys) == count($values)){
                    $arr    =   array();
                    $new_values =   array();
                    $new_values =    array();
                    foreach($values as $val){
                    $new_values[]   =   str_replace('"', '',$val);
                    }
                    
                    for($j=0;$j<count($keys);$j++){
                        if($keys[$j] != ""){
                            $arr[$keys[$j]] =   $new_values[$j];
                        }
                    }

                    $content[$i]=   $arr;
                    $i++;
                }
            }
        }
        fclose($file);
        return $content;
    }

    function escape_string($data){
        $result =   array();
        foreach($data as $row){
            $result[]   =   str_replace('"', '',$row);
        }
        return $result;
    }   

}
?>
