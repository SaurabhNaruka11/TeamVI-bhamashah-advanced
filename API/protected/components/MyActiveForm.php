<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class MyActiveForm extends CActiveForm 
{
    
    public function error($model,$attribute,$htmlOptions=array(),$enableAjaxValidation=true,$enableClientValidation=true)
    {
        $html = '<div class="error-left"></div>';
        $html .= parent::error($model, $attribute, $htmlOptions, $enableAjaxValidation,$enableClientValidatin);
        return $html;
    }
}