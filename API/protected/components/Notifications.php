<?php

/*
 * Purpose  :   To send notification on mobile
 * Author   :   Suman
 */

class Notifications {

    public $notify_limit = 1000;

    public function sendMessageToiOS($iOSContent) {

        $users = Yii::app()->db->createCommand()
                ->select('DISTINCT(device_token)')
                ->from('ca_user_devices')
                ->where('device_type=:device_type AND application_id=:application_id', array(':device_type' => 'iOS',
                    'application_id' => $iOSContent['app_id']))
                ->queryAll();

        $message = $iOSContent['message'];
        $device_tokens = array();

        $batch = $this->createBatch($users, 'device_token');

        if ($batch) {
            foreach ($batch as $device_tokens) {

                $apnsGcm = Yii::app()->apnsGcm;

                $check = $apnsGcm->sendMulti(YiiApnsGcm::TYPE_APNS, $device_tokens, $message, array(
                    'module_identifier' => $iOSContent['module_identifier'],
                    'relation_id' => $iOSContent['relation_id'],
                    'alert' => '',
                        ), array(
                    'sound' => 'default',
                    'badge' => 1
                        )
                );
            }
        }

        return true;
    }

    public function sendMessageToAndroid($androidContent) {
        $apnsGcm = '';
        $users = Yii::app()->db->createCommand()
                ->select('DISTINCT(registration_id)')
                ->from('ca_user_devices')
                ->where('device_type=:device_type AND application_id=:application_id', array(':device_type' => 'Android',
                    'application_id' => $androidContent['app_id']))
                ->queryAll();

        $push_tokens = array();

        $batch = $this->createBatch($users, 'registration_id');

        if ($batch) {
            foreach ($batch as $push_tokens) {

                $message = $androidContent['message'];

                $apnsGcm = Yii::app()->apnsGcm;
                $apnsGcm->sendMulti(YiiApnsGcm::TYPE_GCM, $push_tokens, $message, array(
                    'module_identifier' => $androidContent['module_identifier'],
                    'relation_id' => $androidContent['relation_id'],
                    'notification_image' => ($androidContent['image'] != '') ? $androidContent['image'] : '',
                        ), array(
                    'timeToLive' => 3
                        )
                );
            }
        }
        return $apnsGcm;
    }

    // News Module
    public function androidNewsNotification($androidContent) {
        $apnsGcm = '';
        $users = Yii::app()->db->createCommand()
                ->select('DISTINCT(registration_id)')
                ->from('ca_user_devices')
                ->where('device_type=:device_type AND application_id=:application_id', array(':device_type' => 'Android',
                    'application_id' => $androidContent['app_id']))
                ->queryAll();
        $push_tokens = array();

        $batch = $this->createBatch($users, 'registration_id');

        if ($batch) {
            foreach ($batch as $push_tokens) {

                $message = $androidContent['message'];

                $apnsGcm = Yii::app()->apnsGcm;
                $apnsGcm->sendMulti(YiiApnsGcm::TYPE_GCM, $push_tokens, $message, array(
                    'module_identifier' => $androidContent['module_identifier'],
                    'relation_id' => $androidContent['relation_id'],
                    'news_id' => $androidContent['news_id'],
                    'category_id' => $androidContent['category_id'],
                    'news_type' => $androidContent['news_type'],
                    'notification_image' => ($androidContent['image'] != '') ? $androidContent['image'] : '',
                        ), array(
                    'timeToLive' => 3
                        )
                );
            }
        }
        return $apnsGcm;
    }

    public function iOSNewsNotification($iOSContent) {


        $users = Yii::app()->db->createCommand()
                ->select('DISTINCT(device_token)')
                ->from('ca_user_devices')
                ->where('device_type=:device_type AND application_id=:application_id', array(':device_type' => 'iOS',
                    'application_id' => $iOSContent['app_id']))
                ->queryAll();

        $message = $iOSContent['message'];
        $device_tokens = array();
        $batch = $this->createBatch($users, 'device_token');

        if ($batch) {
            foreach ($batch as $device_tokens) {
                $device_token = implode(',', $device_tokens);
                $apnsGcm = Yii::app()->apnsGcm;
                $check = $apnsGcm->sendMulti(YiiApnsGcm::TYPE_APNS, $device_tokens, $message, array(
                    'module_identifier' => $iOSContent['module_identifier'],
                    'relation_id' => $iOSContent['relation_id'],
                    'news_id' => $iOSContent['news_id'],
                    'category_id' => $iOSContent['category_id'],
                    'news_type' => $iOSContent['news_type'],
                    'alert' => '',
                        ), array(
                    'sound' => 'default',
                    'badge' => 1
                        )
                );
            }
        }

        return true;
    }

    // Food Order Module
    public function foodOrderAndroidNotification($androidContent, $device_id) {
        $apnsGcm = '';
        $users = Yii::app()->db->createCommand()
                ->select('*')
                ->from('ca_user_devices')
                ->where('device_type=:device_type AND application_id=:app_id AND '
                        . 'device_id=:device_id', array(':device_type' => 'Android',
                    'app_id' => Yii::app()->user->app_id, ':device_id' => $device_id))
                ->queryAll();
        //send notification to all registered users
        $push_tokens = array();
        $batch = $this->createBatch($users, 'registration_id');

        if ($batch) {
            foreach ($batch as $push_tokens) {

                $message = $androidContent['message'];

                $apnsGcm = Yii::app()->apnsGcm;
                $apnsGcm->sendMulti(YiiApnsGcm::TYPE_GCM, $push_tokens, $message, array(
                    'module_identifier' => $androidContent['module_identifier'],
                    'relation_id' => $androidContent['relation_id'],
                    'notification_image' => ($androidContent['image'] != '') ? $androidContent['image'] : '',
                        ), array(
                    'timeToLive' => 3
                        )
                );
            }
        }
        return $apnsGcm;
    }

    public function foodOrderIOSNotification($iOSContent, $device_id) {

        $users = Yii::app()->db->createCommand()
                ->select('device_token')
                ->from('ca_user_devices')
                ->where('device_type=:device_type AND application_id=:app_id AND device_id=:device_id', array(':device_type' => 'iOS', 'app_id' => Yii::app()->user->app_id, ':device_id' => $device_id))
                ->queryAll();

        $message = $iOSContent['message'];
        $device_tokens = array();

        $batch = $this->createBatch($users, 'device_token');

        if ($batch) {
            foreach ($batch as $device_tokens) {

                $apnsGcm = Yii::app()->apnsGcm;

                $check = $apnsGcm->sendMulti(YiiApnsGcm::TYPE_APNS, $device_tokens, $message, array(
                    'module_identifier' => $iOSContent['module_identifier'],
                    'relation_id' => $iOSContent['relation_id'],
                    'alert' => '',
                        ), array(
                    'sound' => 'default',
                    'badge' => 1
                        )
                );
            }
        }

        return true;
    }

    // To create a bath of 1000 users as GCM send notification to 0nly 1000 devices at a time 
    public function createBatch($users, $field_name) {

        $batch = array();
        $i = 0;
        foreach ($users as $user) {
            if (count($batch) > 0 && count($batch[$i]) == $this->notify_limit) {
                $i++;
            }
// To check device token is valid or not for iOS
            if (($field_name == 'device_token' && preg_match('~^[a-f0-9]{64}$~i', $user[$field_name])) || $field_name == 'registration_id')
                $batch[$i][] = $user[$field_name];
        }

        return $batch;
    }

}
