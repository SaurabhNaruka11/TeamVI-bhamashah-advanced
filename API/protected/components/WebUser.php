<?php
class WebUser extends CWebUser
{
    public function isAdmin()
    {
        return ($this->getState('user_role')==Yii::app()->params->admin_role_id);
    }
    
    public function isReseller()
    {
        return ($this->getState('user_role')==Yii::app()->params->normal_reseller_role_id || $this->getState('user_role')==Yii::app()->params->white_label_reseller_role_id);
    }
    
    public function isSalesPerson()
    {
        return ($this->getState('user_role')==Yii::app()->params->sales_person_role_id);
    }
    
    public function isMerchant()
    {
        return($this->getState('user_role')==Yii::app()->params->merchant_role_id);
    }
    public function isSuperAdmin()
    {
        return($this->getState('user_role')==Yii::app()->params->super_admin_role_id);
    }
}
?>
