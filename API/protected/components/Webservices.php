<?php

class Webservices {

    public function home($application_id, $type) {
        $commonfunction = new CommonFunctions();
        $models1 = AppHome::model()->app_home($application_id, $type);
        $mod1 = array_fill_keys(array('app-home'), $models1);
        $models2 = AppConfig::model()->app_config($application_id);
        $mod2 = array_fill_keys(array('app-config'), $models2);
        if (count($models1) > 1) {
            $mod2['app-config']['logo'] = $mod1['app-home']['logo'];
        } else {
            $mod2['app-config']['logo'] = "";
        }
        $models3 = Modules::model()->app_module($application_id);
        $mod3 = array_fill_keys(array('app-module'), $models3);

        $merchant_info = $commonfunction->getMerchantInfo($application_id);
        if (!empty($merchant_info)) {
            if (empty($merchant_info['contact_number'])) {
                $contact_number = "";
            } else {
                $contact_number = $merchant_info['contact_number'];
            }
            $models4 = array('username' => $merchant_info['username'],
                'email' => $merchant_info['email'],
                'name' => $merchant_info['first_name'] . ' ' . $merchant_info['last_name'],
                'contact_number' => $contact_number);
        } else {
            $models4 = array();
        }
        $mod4 = array_fill_keys(array('merchant-info'), $models4); 
        $models = array_merge($mod3, $mod2, $mod1, $mod4);      
        
        
        //Code for beacon
        $beaconList = Modules::model()->getBeaconDevices($application_id);
        if($beaconList)
        {
            $beacon_details = array(
                'devices'=>$beaconList
            );  
            $final_beacons = array_fill_keys(array('beacons-info'), $beacon_details);
            $models = array_merge($models,$final_beacons);
        }
       
        //Code for Accuware
        $accuwareInfo = Accuware::model()->getAccuwareInfo($application_id);
        if($accuwareInfo){
            $final_accuware = array_fill_keys(array('accuware-info'), $accuwareInfo);
            $models = array_merge($models,$final_accuware);
        }

        //code for messages count
        $data_messages = Messages::model()->getMessages($application_id);
        $count_messages = $data_messages ? count($data_messages) : 0;
        $models['messages_count'] = $count_messages;
        
        //Code for magento details
        if (Yii::app()->params->Magento['is_magento_enabled'] == TRUE) {
        $magento_details = array();
            $magento_details = $commonfunction->getDBData('*', 'app_id', $application_id, 'ca_estore_magento', 'row');
            $magento_details['is_magento'] = $magento_details ? TRUE : FALSE;
            $default_outlet = AroundusOutlets::model()->findByAttributes(array('application_id' => $application_id), 
                    'is_default=:is_default AND is_publish =:is_publish', array(':is_default' => 1, ':is_publish'=>1));

        $models['magento_details'] = $magento_details;
            if ($default_outlet) {
                $models['magento_details']['location'] = array(
                    'outlet_name' => $default_outlet['outlet_name'],
                    'outlet_address' => $default_outlet['outlet_address'],
                    'latitude' => $default_outlet['latitude'],
                    'longitude' => $default_outlet['longitude'],
                );
            }
        }
        $models['emailValidateKey']= Yii::app()->params['emailValidateKey'];
        $value1 = count($models['app-module']);
        $value2 = count($models['app-config']);
        $value3 = count($models['app-home']['gallery']);
        $value4 = count($models['merchant-info']);
        
        if ($value1 == 0 && $value2 == 1 && $value3 == 0 && $value4 == 0) {
            return array(
                'status' => "0",
                "success" => "",
                "info" => "",
                "error" => "No Result Found."
            );
        } else {
            return array(
                'status' => "1",
                "success" => "success",
                "error" => "",
                "info" => $models
            );
        }
    }

}
