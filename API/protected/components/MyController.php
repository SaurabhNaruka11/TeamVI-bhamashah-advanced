<?php

class MyController extends CController {

    function init() {
        parent::init();
        $app = Yii::app();
        if (isset($_POST['_lang'])) {
            $app->language = $_POST['_lang'];
            $app->session['_lang'] = $app->language;
        } else if (isset($app->session['_lang'])) {
            $app->language = $app->session['_lang'];
        }
    }

    public function handleError(CEvent $event) {

        if ($event instanceof CExceptionEvent) {
            $controller = Yii::app()->controller->id;
            $action = Yii::app()->controller->action->id;

            $url = Yii::app()->request->requestUri;
            if (Yii::app()->user->isGuest || !isset(Yii::app()->user->user_id)) {
                return $this->redirect(array('users/login'));
            } else {
//                $autoEmail = new AutoEmail();
//                $autoEmail->sendErrorToDeveloper($event, $url, $controller, $action);
            }
        } elseif ($event instanceof CErrorEvent) {
        }

        $event->handled = TRUE;
    }

}

?>