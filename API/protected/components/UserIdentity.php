<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity
        extends CUserIdentity {

    const ERROR_NONE = 0;
    const ERROR_EMAIL_INVALID = 1;
    const ERROR_PASSWORD_INVALID = 2;
    const ERROR_USER_NOT_ACTIVE = 3;
    const UN_AUTHENTICATE_USER = 4;
    const ERROR_USER_EXPIRED = 5;

    public $email, $username;

    public function authenticate_user($email) {
        $commonfunction = new CommonFunctions();
        $this->errorCode = self::ERROR_NONE;
        $record = Users::model()->findByAttributes(array('email' => $email));
        if (isset($record)) {
            $user_role = $commonfunction->getUserRole($record->user_id);
            $is_expired = $commonfunction->checkMerchantExpiry($record->user_id);
        }
        if (count($record) > 0) {
            if (!isset($email)) {
                $this->errorCode = self::ERROR_EMAIL_INVALID;
            } elseif ($record->password != $this->password) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } elseif ($record->is_active == '0') {
                $this->errorCode = self::ERROR_USER_NOT_ACTIVE;
            } 
//            elseif ($user_role == Yii::app()->params->merchant_role_id && $is_expired == TRUE) {
//                $this->errorCode = self::ERROR_USER_EXPIRED;

//            } 
            else {
                $user_role = $commonfunction->getUserRole($record->user_id);
                if ($user_role == Yii::app()->params->sales_person_role_id) {
                    $this->errorCode = self::ERROR_EMAIL_INVALID;
                    return $this->errorCode;
                }
                Yii::app()->user->setState("user_role", $user_role);
                Yii::app()->user->setState("user_id", $record->user_id);
                Yii::app()->user->setState("username", $record->username);
                Yii::app()->user->setState("email", $record->email);
                if ($user_role == Yii::app()->params->admin_role_id) {
                    Yii::app()->user->setState("name", $record->contact_person);
                } else {
                    ($user_role == 3) ? (Yii::app()->user->setState("user_type", 'NR')) : '';
                    Yii::app()->user->setState("name", $record->first_name);
                }
                if ($user_role != Yii::app()->params->merchant_role_id) {
                    $user_type = Yii::app()->params->user_types[$user_role];
                } else {
                    $parent = Users::model()->findByPK($record->user_id)->parent_id;
                    $user_admin = Users::model()->findByPK($record->user_id)->user_admin;
                    

                    $command = Yii::app()->db->createCommand();
                    $res = $command->select('role_id')->from('ca_user_roles')
                            ->where('user_id=:user_id', array(':user_id' => $user_admin))
                            ->queryRow();
                    $command->reset();


                    if ($parent != NULL && $parent == 2 && $user_admin != 0) {
                        //if($parent!=NULL && $parent!=2)
                        $user_type = 'RM';
                    } else {
                        $user_type = 'DM';
                    }
                    if (in_array($res['role_id'], array(1, 2, 3, 6))) {
                        $user_type = 'RM';
                    } else {
                        $user_type = 'DM';
                    }
                }
                Yii::app()->user->setState("user_type", $user_type);
                Yii::app()->session['_lang'] =  Yii::app()->language;
                $this->errorCode = self::ERROR_NONE;
            }
        } else {
            $this->errorCode = self::ERROR_EMAIL_INVALID;
        }
        return $this->errorCode;
    }

    //function for autheticate admin
    public function authenticate_admin($email) {
        $commonfunction = new CommonFunctions();
        $this->errorCode = self::ERROR_NONE;
        //$email represents $this->email
        $record = Users::model()->findByAttributes(array('email' => $email));
        if (count($record) == 0) {
            $this->errorCode = self::ERROR_EMAIL_INVALID;
            return $this->errorCode;
        }
        $get_admin_role = Yii::app()->params->admin_role_id;
        $get_super_admin_role = Yii::app()->params->super_admin_role_id;
        $match_role = UserRoles::model()->findByAttributes(array('user_id' => $record->user_id));
        if (!isset($email)) {
            $this->errorCode = self::ERROR_EMAIL_INVALID;
        } elseif ($record->password !== $this->password) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else if ($match_role != "" && $get_admin_role != $match_role->role_id && $get_super_admin_role != $match_role->role_id) {
            $this->errorCode = self::UN_AUTHENTICATE_USER;
        } else if ($match_role->role_id != $get_super_admin_role && $record->parent_id != $get_super_admin_role) {
            $this->errorCode = self::ERROR_USER_NOT_ACTIVE;
        } else {
            Yii::app()->user->setState("user_id", $record->user_id);
            Yii::app()->user->setState("username", $record->username);
            Yii::app()->user->setState("email", $record->email);
            Yii::app()->user->setState("name", $record->contact_person);
            $user_role = $commonfunction->getUserRole($record->user_id);
            Yii::app()->user->setState("user_role", $user_role);


            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode;
    }

    public function authenticate_reseller($email) {
        $commonfunction = new CommonFunctions();
        $this->errorCode = self::ERROR_NONE;
        $record = Users::model()->findByAttributes(array('email' => $email));

        if (count($record) > 0) {
            if (!isset($email)) {
                $this->errorCode = self::ERROR_EMAIL_INVALID;
            } elseif ($record->password != $this->password) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } elseif ($record->is_active == '0') {
                $this->errorCode = self::ERROR_USER_NOT_ACTIVE;
            } else {
                Yii::app()->user->setState("user_id", $record->user_id);
                Yii::app()->user->setState("email", $record->email);
                Yii::app()->user->setState("name", $record->first_name);
                $user_role = $commonfunction->getUserRole($record->user_id);
                Yii::app()->user->setState("user_role", $user_role);
                Yii::app()->user->setState("user_type", 'NR');

                $this->errorCode = self::ERROR_NONE;
            }
        } else {
            $this->errorCode = self::ERROR_EMAIL_INVALID;
        }
        return $this->errorCode;
    }

    // Authenticate salesperson for backend login
    public function authenticate_salesperson($email) {
        $commonfunction = new CommonFunctions();
        $this->errorCode = self::ERROR_NONE;
        $record = Users::model()->findByAttributes(array('email' => $email));
        $salesperson_id = Roles::model()->findByAttributes(array('rolename' => 'SALES_PERSON'));
        $user_role_id = $commonfunction->getUserRole($record['user_id']);
        if (count($record) > 0) {
            if (!isset($email)) {
                $this->errorCode = self::ERROR_EMAIL_INVALID;
            } elseif ($record->password != $this->password) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } elseif ($record->is_active == '0') {
                $this->errorCode = self::ERROR_USER_NOT_ACTIVE;
            } elseif ($user_role_id != $salesperson_id->role_id) {
                $this->errorCode = self::UN_AUTHENTICATE_USER;
            } else {
                Yii::app()->user->setState("user_id", $record->user_id);
                Yii::app()->user->setState("username", $record->username);
                Yii::app()->user->setState("email", $record->email);
                Yii::app()->user->setState("name", $record->contact_person);
                Yii::app()->user->setState("user_role", $user_role_id);
                Yii::app()->user->setState("user_type", 'SP');
                $this->errorCode = self::ERROR_NONE;
            }
        } else {
            $this->errorCode = self::ERROR_EMAIL_INVALID;
        }
        return $this->errorCode;
    }

}
?>

