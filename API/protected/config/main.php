<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'CreateApp',
    'id' => 'create_app_v2',
    'language' => 'en',
    'preload' => array('log'),
    'theme' => 'create_app',
    'defaultController' => 'users/index',
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.easyPaypal.*',
        'application.components.widgets.*',
        'application.components.widgets.uploadify.*',
        'application.components.widgets.uploadify.assets.*',
        'application.extentions.Jeapie.*',
        'application.vendor.Instagram.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'password',
            'ipFilters' => false,
        ),
    ),
    // Added by Ankit Arora on 12 April, 2014 to set default timezone of Singapore - Starts Here
    'timeZone' => 'Asia/Singapore'
    ,
    // Added by Ankit Arora on 12 April, 2014 to set default timezone of Singapore - Ends Here
    // application components 
    'components' => array(
//         'cache' => array(
//             'class' => 'CApcCache',
//         ),
        'user' => array(
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'authTimeout' => 60 * 90,
        ),
        'session' => array(
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
            'timeout' => 5400,
        ),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "tls://email-smtp.us-east-1.amazonaws.com",
            'Username' => 'AKIAJY6ILDCHNY32NGNA',
            'Password' => 'AqaPAbbl6yVVdxAA4zmgvxrZXf1Os1z+7HZKNo2dGZiV',
            'Mailer' => 'smtp',
            'Port' => 465,
            'SMTPAuth' => true,
        ),
        'apns' => array(
            'class' => 'ext.apns-gcm.YiiApns',
            'environment' => 'production',
            'dryRun' => false, // setting true will just do nothing when sending push notification
            'options' => array(
                'sendRetryTimes' => 3
            ),
        ),
        'gcm' => array(
            'class' => 'application.extensions.apns-gcm.YiiGcm',
            'apiKey' => 'AIzaSyCkN_ZYh0I6e6sFJVVbB_I6eLvgdlEXJM4'
        ),
        // using both gcm and apns, make sure you have 'gcm' and 'apns' in your component
        'apnsGcm' => array(
            'class' => 'application.extensions.apns-gcm.YiiApnsGcm'
        ),
        // Added by Ankit Arora on 12 April, 2014 to set SMTP settings for Email - Ends Here
        'file' => array(
            'class' => 'application.extensions.file.CFile',
        ),
        's3' => array(
            'class' => 'ext.aws.ES3',
            'aKey' => 'AKIAIT5YHQ6Q73AWX7TQ',
            'sKey' => '1jx3jLvuYebyjGvxd2LlzcZNVnH6eT3xGe7WJIla',
            'bucket' => 'createapp2-dev',
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.extensions.mpdf.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class' => 'mpdf',
                )
            ),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'posts/<tag:.*?>' => 'post/index',
                array('api/list', 'pattern' => 'api/<model:\w+>/*', 'verb' => null),
                array('api/insert', 'pattern' => 'api/<model:\w+>/*', 'verb' => null),
                array('api/view', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => null),
                array('api/update', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => null), // Update
                array('api/delete', 'pattern' => 'api/<model:\w+>/<id:\d+>', 'verb' => null),
                array('api/create', 'pattern' => 'api/<model:\w+>', 'verb' => null), // Create  
                'Free-Sign-Up' => 'users/create',
                'Resellers-Free-Sign-Up' => 'resellers/create',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'add-coupon/<module:\S+>' => 'couponCode/create',
                'coupon' => 'couponCode/index',
                'coupons/<module:\S+>' => 'couponCode/admin',
                'update-coupon/<module:\S+>/<id:\S+>' => 'couponCode/update',
                'view-coupon/<module:\S+>/<id:\S+>' => 'couponCode/view',
                'delete-coupon/<module:\S+>/<id:\S+>' => 'couponCode/delete',
            ),
        ),
        'db' => array(
//            'connectionString' => 'mysql:host=createappdb-restore.cukt1t3atqi3.ap-southeast-1.rds.amazonaws.com;dbname=createapp_staging',
//            'username' => 'createappuser',
//            'password' => 'createapp_2013',
//            'connectionString' => 'mysql:host=192.168.10.170;dbname=createapp_repair',
            'connectionString' => 'mysql:host=localhost;dbname=raj',
            'username' => 'root',
            'password' => '',
            'emulatePrepare' => true,
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            'errorAction' => 'applications/features',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'params' => array(
        'appuserloginurl' => 'http://appuserlogin.com',
        's3baseUrl' => 'http://createapp2-dev.s3.amazonaws.com',
        'adminEmail' => 'info@createappasia.com',
        'resellerEmail' => 'info@appuserlogin.com',
        'developersEmail' => array('saurabh.naruka@ranosys.com', 'amit.sharma@ranosys.com'),
        'ToDevelopers' => 'saurabh.naruka@ranosys.com',
        
//        'mangersEmail' => array('palak@createappasia.com', 'eddie_fwk@yahoo.com'),
        'mangersEmail' => array('saurabh.naruka@ranosys.com'),
//        'toEmail' => 'info@createappasia.com',
        'toEmail' => 'rtpl.developer@ranosys.com',
        'frontEndAssetsUrl' => (strlen(dirname($_SERVER['SCRIPT_NAME'])) > 1 ? dirname($_SERVER['SCRIPT_NAME']) : '' ) . '/themes/front_end/assets/',
        'uploadsUrl' => 'http://' . $_SERVER['HTTP_HOST'] . (strlen(dirname($_SERVER['SCRIPT_NAME'])) > 1 ? dirname($_SERVER['SCRIPT_NAME']) : '' ) . '/uploads/',
        'AppBaseURL' => 'http://' . $_SERVER['HTTP_HOST'] . (strlen(dirname($_SERVER['SCRIPT_NAME'])) > 1 ? dirname($_SERVER['SCRIPT_NAME']) : '' ),
        //Paypal Details
        'PAYPAL_API_USERNAME' => 'createappv2testing_api1.gmail.com',
        'PAYPAL_API_PASSWORD' => 'CAXD4EK3AEUXQZ5B',
        'PAYPAL_API_SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AycIEFN.VsO23pwMJVK.w9n.6tSZ',
        'PAYPAL_MODE' => 'sandbox',
        //'PAYPAL_API_USERNAME' => 'eddiefoong_api1.createappsingapore.com',
        //'PAYPAL_API_PASSWORD' => 'K6RUXH5JY4K56MK7',
        //'PAYPAL_API_SIGNATURE' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AI4MWeW8L63bFGoMRinCjrstiI.e',
        //'PAYPAL_MODE' => 'live',
        'normal_reseller_apps' => array('10' => '10', '30' => '30', '50' => '50', '100' => '100', '200' => '200'),
        'reseller_apps_price' => array('10' => '200', '30' => '300', '50' => '400', '100' => '750', '200' => '1200'),
        'white_lable_reseller_apps' => array('30' => '30', '50' => '400', '100' => '100', '200' => '200'),
        'default_currency_website' => 'USD',
        'payment_methods' => array("is_paypal" => "1", "is_COD" => "0", "is_2C2P" => "0"),
        //2C2P info
        'default_currency_2c2p' => 'IDR',
        'merchant_id_2c2p' => '702702000000011',
        'secret_key_2c2p' => '5NF4g7ysJt4K',
        'currency_code_2c2p' => '360',
        'country_code_2c2p' => 'ID',
        'is_2c2p_payment' => TRUE,
        'payment_url_2c2p' => 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/PaymentAuth.aspx',
        //Roled id Parameters
        'super_admin_role_id' => '1',
        'admin_role_id' => '2',
        'normal_reseller_role_id' => '3',
        'sales_person_role_id' => '4',
        'merchant_role_id' => '5',
        'white_label_reseller_role_id' => '6',
        'per_application_price' => '0.1',
        'per_application_price_Monthly' => '65',
        'per_application_price_Yearly' => '540',
        'icon_folders' => '20',
        'user_types' => array(
            '1' => 'SA',
            '2' => 'A',
            '3' => 'NR',
            '4' => 'SP',
            '5' => 'DM',
            '6' => 'WR',
        ),
        'home_buttons' => array(array('title' => "Call Us", 'background_color' => "#cd091f", 'text_color' => "#ffffff", 'is_active' => 1, 'identifier' => "contactus"),
            array('title' => "Direction", 'background_color' => "#016aab", 'text_color' => "#ffffff", 'is_active' => 1, 'identifier' => "contactus"),
            array('title' => "Tell A Friend", 'background_color' => "#5a5a5a", 'text_color' => "#ffffff", 'is_active' => 1, 'identifier' => "tellfriend"),
        ),
        'document_type' => array(
            'image' => 'Image',
            'text' => 'Text',
            'doc' => 'Doc',
            'csv' => 'Csv',
            'pdf' => 'PDF',
            'other' => 'Other'
        ),
        's3bucket_name' => 'createapp2-dev',
        'regex' => array('^([1-9]{1})(\d{3})$', '^(?!01000|99999)(0[1-9]\d{3}|[1-9]\d{4})$', '^([1-9]{1}[0-9]{3})$', '^(\d{2})([\.]?)(\d{3})([\-]?)(\d{3})$',
            '^(?:A|B|C|E|G|H|J|K|L|M|N|P|R|S|T|V|X|Y){1}[0-9]{1}(?:A|B|C|E|G|H|J|K|L|M|N|P|R|S|T|V|W|X|Y|Z){1}\s?[0-9]{1}(?:A|B|C|E|G|H|J|K|L|M|N|P|R|S|T|V|W|X|Y|Z){1}[0-9]{1}$',
            '^([1-9]{1})(\d{3})$',
            '^(\d{3})([ ]?)(\d{2})$',
            '^(DK(-|\s)?)?\d{4}$',
            '^(?:0[1-9]|[1-4][0-9]|5[0-2])\d{3}$',
            '^[0-9]{5}$',
            '^\d{3}\s?\d{3}$',
            '^(D6W|[ACDEFHKNPRTVWXY]\d{2})\s[0-9ACDEFHKNPRTVWXY]{4}$',
            '^(I-|IT-)?\d{5}$',
            '^[1-9][0-9]{4}$',
            '^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$',
            '^[0-9]{2}\-[0-9]{3}$',
            '^[1-9]\d{3}-\d{3}$',
            '^(0[1-8]{1}|[1-9]{1}[0-5]{1})?[0-9]{4}$',
            '^[0-9]{6}$',
            '^(S-)?\d{3}\s?\d{2}$',
            '^([0][1-9]|[1-6][0-9]|[7]([0-3]|[5-9])|[8][0-2])(\d{4})$',
            '^(\d{3})([ ]?)(\d{2})$',
            '^\d{4,5}([\-]?\d{4})?$'
        ),
        'paypal_supportive_currency' => array('USD', 'SGD', 'EUR', 'AUD', 'CAD', 'BRL',
            'HKD', 'ILS', 'MXN', 'NZD', 'SEK', 'THB', 'CZK', 'DKK',
            'NOK', 'PHP', 'PLN', 'GBP', 'CHF', 'HUF', 'JPY', 'TWD', 'TRY', 'MYR', 'BRL'),
        'paypal_incountry_currency' => array('TRY', 'MYR', 'BRL'),
        'paypal_decimal_unsupport_curr' => array('HUF', 'JPY', 'TWD'),
        'all_currency' => array('USD', 'SGD', 'EUR', 'AUD', 'CAD', 'BRL',
            'HKD', 'ILS', 'MXN', 'NZD', 'SEK', 'THB', 'CZK', 'DKK',
            'NOK', 'PHP', 'PLN', 'GBP', 'CHF', 'HUF', 'JPY', 'TWD', 'TRY',
            'MYR', 'BRL', 'INR', 'CNY', 'IDR'),
        'beacon_title' => 'Beacons',
        'beacon_proximity' => 'IMMEDIATE',
        'beacon_url' => 'https://api.kontakt.io/',
        'default_beacon_key' => 'DZEFYaIPLxMhgTUjkYsyIThVJVjzJtwL',
        'website_name' => 'CreateApp-Staging',
        'copyright_year' => '2015',
        'show_reseller' => TRUE,
        'show_aboutus' => TRUE,
        'show_winnerlogo' => TRUE,
        'address_title' => 'CreateApp',
        'address' => '<br /> 420 North Bridge Road #06 - 29/30 <br/> North Bridge Centre <br/> Singapore 188727 <br/>',
        'facebook_url' => 'https://www.facebook.com/pages/Createappsingaporecom-Singapore-1-Mobile-App-Developer-Program/327672557365493',
        'twitter_url' => 'https://twitter.com/CreateAppSg',
        'googleplus_url' => 'https://plus.google.com/u/0/107997791950304578350/posts',
        'linkedin_url' => 'https://www.linkedin.com/pub/createapp-asia/101/746/a06',
        //Preview/index.php
        'preview_android_apk_url' => 'http://staging.createappasia.com/builds/CreateAppAsiaStaging.apk',
        'preview_ios_build_url' => 'https://createappsingapore.com/CreateAppAsiaStaging/download.html',
        'preview_ios_id' => '50nmcm8ne1e9z8z1m87xz68qtm',
        'preview_kit_key' => '50nmcm8ne1e9z8z1m87xz68qtm',
        'payment_methods' => array('is_paypal' => '1', 'is_COD' => '0', 'is_2c2p' => '0'),
        //YiiApns
        'global_apns_pem' => 'CreateAppV2AsiaStaging_ck.pem',
        /** Template Module Params */
//        'base_app_templates' => array('125', '1069', '1070', '117', '125', '267', '268'),
        'base_app_templates' => array('259', '108', '109', '117', '125', '267', '268'),
        'restaurant_temp' => '259',
//        'restaurant_temp' => '1070', //restaurant_temp
        'hotel_temp' => '108',
        'school_temp' => '109',
//        'school_temp' => '1069',//schools_temp
        'collage_temp' => '117',
//        'collage_temp' => '125', //staging
        'clinic_temp' => '125',
        'wedding_temp' => '267',
        'property_temp' => '268',
        //default template app_code
        'default_parent_appcode' => 'PbDezR',
//        'default_parent_appcode' => 'slvqpu',//staging
        'defaultPageSize' => 10,
         'disableCopyModules'=>array(1,7,8,10,11,12,14,24,29,31,32,36,41,45),
        'emailValidateKey' => 'deb39c1695a00330fecd12a445f0afd7',
    ),
     
);
