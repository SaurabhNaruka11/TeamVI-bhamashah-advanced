<?php

class ApiController extends MyController {

    Const APPLICATION_ID = 'ASCCPE';

    private $format = 'json';

    public function filters() {
        return array();
    }

    public function actionIndex() {
        echo CJSON::encode(array(1, 2, 3));
    }

    public function actionList() {
        $Newheaders = $this->get_nginx_headers();
        Yii::app()->language = (isset($Newheaders['Lang-Code'])) ? $Newheaders['Lang-Code'] : 'en';
        switch ($_GET['model']) {
            
            /*
             * For listing all the active users
             */
            case 'users':
//                $user_id = $_REQUEST['client_id'];
                $models = User::model()->getUsers();
                $this->_sendResponse(200, CJSON::encode($models));
                break;
            
            /*
             * For getting getails of the user as per the client Id/Bhamashah ID
             */

            case 'user_details':
//                echo base64_encode(1);die;
                $user_id = base64_decode($_REQUEST['client_id']);
                $models = User::model()->getUserDetails($user_id);
                $this->_sendResponse(200, CJSON::encode($models));
                break;
            
            /*
             * For getting all the products available on the store
             */
            case 'getProducts':
//                echo base64_encode(1);die;
                $user_id = base64_decode($_REQUEST['client_id']);
                $models = Product::model()->getProducts($user_id);
                $this->_sendResponse(200, CJSON::encode($models));
                break;
            /*
             * For getting all the prducts added by the logged in user
             */
            case 'getUserAddedProducts':
//                echo base64_encode(1);die;
                $user_id = base64_decode($_REQUEST['client_id']);
                $models = Product::model()->getUserAddedProducts($user_id);
                $this->_sendResponse(200, CJSON::encode($models));
                break;
            
            /*
             * For showing order history of purchased product.
             */
            
            case 'getOrdersHistory':
//                echo base64_encode(2);die;
                $user_id = base64_decode($_REQUEST['client_id']);
                $models = Orders::model()->getOrdersHistory($user_id);
                $this->_sendResponse(200, CJSON::encode($models));
                break;
            case 'getMarchantOrders':
//                echo base64_encode(1);die;
                $user_id = base64_decode($_REQUEST['client_id']);
                $models = Orders::model()->getMarchantOrders($user_id);
                $this->_sendResponse(200, CJSON::encode($models));
                break;
            default:
                $this->_sendResponse(501, sprintf('Error: Mode <b>list</b> is not implemented for model <b>%s</b>', $_GET['model']));
                exit;
        }



    }

    /**
     * Sends the API response 
     * @param int $status 
     * @param string $body 
     * @param string $content_type 
     * @access private
     * @return void
     */
    private function _sendResponse($status = 200, $body = '', $content_type = 'application/json') {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        // set the status
        header($status_header);
        // set the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
            exit;
        }
        // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            switch ($status) {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templatized in a real-world solution
            $body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                        <html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                                <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                            </head>
                            <body>
                                <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                                <p>' . $message . '</p>
                                <hr />
                                <address>' . $signature . '</address>
                            </body>
                        </html>';

            echo $body;
            exit;
        }
    }

    /**
     * Gets the message for a status code
     * @param mixed $status 
     * @access private
     * @return string
     */
    private function _getStatusCodeMessage($status) {
        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    /**
     * Checks if a request is authorized
     * @access private
     * @return void
     */
    private function _checkAuth() {
        if (!(isset($_SERVER['HTTP_X_' . self::APPLICATION_ID . '_USERNAME']) and isset($_SERVER['HTTP_X_' . self::APPLICATION_ID . '_PASSWORD']))) {
            $this->_sendResponse(401);
        }
        $username = $_SERVER['HTTP_X_' . self::APPLICATION_ID . '_USERNAME'];
        $password = $_SERVER['HTTP_X_' . self::APPLICATION_ID . '_PASSWORD'];
        $user = User::model()->find('LOWER(username)=?', array(strtolower($username)));
        if ($user === null) {
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if (!$user->validatePassword($password)) {
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }

    /**
     * Returns the json or xml encoded array
     * @param mixed $model 
     * @param mixed $array Data to be encoded
     * @access private
     * @return void
     */
    private function _getObjectEncoded($model, $array) {
        if (isset($_GET['format']))
            $this->format = $_GET['format'];

        if ($this->format == 'json') {
            return CJSON::encode($array);
        } elseif ($this->format == 'xml') {
            $result = '<?xml version="1.0">';
            $result .= "\n<$model>\n";
            foreach ($array as $key => $value)
                $result .= "    <$key>" . utf8_encode($value) . "</$key>\n";
            $result .= '</' . $model . '>';
            return $result;
        } else {
            return;
        }
    }

    function get_nginx_headers($function_name = 'getallheaders') {

        $all_headers = array();

        if (function_exists($function_name)) {

            $all_headers = $function_name();
        } else {

            foreach ($_SERVER as $name => $value) {

                if (substr($name, 0, 5) == 'HTTP_') {

                    $name = substr($name, 5);
                    $name = str_replace('_', ' ', $name);
                    $name = strtolower($name);
                    $name = ucwords($name);
                    $name = str_replace(' ', '-', $name);

                    $all_headers[$name] = $value;
                } elseif ($function_name == 'apache_request_headers') {

                    $all_headers[$name] = $value;
                }
            }
        }


        return $all_headers;
    }

}

?>
