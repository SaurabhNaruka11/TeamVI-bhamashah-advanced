<?php

/**
 * This is the model class for table "oreders".
 *
 * The followings are the available columns in table 'oreders':
 * @property integer $id
 * @property integer $buyer_id
 * @property string $name
 * @property string $address
 * @property string $location_name
 * @property string $city_name
 * @property integer $pincode
 * @property string $email
 * @property string $mobile
 * @property string $comments
 * @property integer $payment_by
 * @property integer $booking_status
 * @property string $total_price
 * @property double $paid_amount
 * @property string $subtotal
 * @property string $delivery_charges
 * @property string $tax_price
 * @property string $order_number
 * @property string $transaction_id
 * @property string $payment_date
 * @property string $created_at
 * @property string $payment_status
 * @property string $cancellation_reason
 * @property string $cancellation_date
 */
class Orders extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oreders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('buyer_id, name, address, location_name, city_name, pincode, email, mobile, comments, payment_by, booking_status, total_price, paid_amount, subtotal, delivery_charges, tax_price, order_number, transaction_id, payment_date, created_at, payment_status, cancellation_reason', 'required'),
			array('buyer_id, pincode, payment_by, booking_status', 'numerical', 'integerOnly'=>true),
			array('paid_amount', 'numerical'),
			array('name, email, transaction_id', 'length', 'max'=>255),
			array('location_name, city_name', 'length', 'max'=>100),
			array('mobile', 'length', 'max'=>15),
			array('total_price, subtotal, delivery_charges, tax_price', 'length', 'max'=>10),
			array('order_number', 'length', 'max'=>23),
			array('payment_status', 'length', 'max'=>50),
			array('cancellation_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, buyer_id, name, address, location_name, city_name, pincode, email, mobile, comments, payment_by, booking_status, total_price, paid_amount, subtotal, delivery_charges, tax_price, order_number, transaction_id, payment_date, created_at, payment_status, cancellation_reason, cancellation_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'buyer_id' => 'Buyer',
			'name' => 'Name',
			'address' => 'Address',
			'location_name' => 'Location Name',
			'city_name' => 'City Name',
			'pincode' => 'Pincode',
			'email' => 'Email',
			'mobile' => 'Mobile',
			'comments' => 'Comments',
			'payment_by' => 'Payment By',
			'booking_status' => 'Booking Status',
			'total_price' => 'Total Price',
			'paid_amount' => 'Paid Amount',
			'subtotal' => 'Subtotal',
			'delivery_charges' => 'Delivery Charges',
			'tax_price' => 'Tax Price',
			'order_number' => 'Order Number',
			'transaction_id' => 'Transaction',
			'payment_date' => 'Payment Date',
			'created_at' => 'Created At',
			'payment_status' => 'Payment Status',
			'cancellation_reason' => 'Cancellation Reason',
			'cancellation_date' => 'Cancellation Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('buyer_id',$this->buyer_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('location_name',$this->location_name,true);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('pincode',$this->pincode);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('payment_by',$this->payment_by);
		$criteria->compare('booking_status',$this->booking_status);
		$criteria->compare('total_price',$this->total_price,true);
		$criteria->compare('paid_amount',$this->paid_amount);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('delivery_charges',$this->delivery_charges,true);
		$criteria->compare('tax_price',$this->tax_price,true);
		$criteria->compare('order_number',$this->order_number,true);
		$criteria->compare('transaction_id',$this->transaction_id,true);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('payment_status',$this->payment_status,true);
		$criteria->compare('cancellation_reason',$this->cancellation_reason,true);
		$criteria->compare('cancellation_date',$this->cancellation_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Oreders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getMarchantOrders($client_id) {

        $result = Yii::app()->db->createCommand()
                ->select('p.*,u.full_name')
                ->from('oreders p')
                ->leftJoin('user u', 'u.id = p.buyer_id')
                ->queryAll();
        if ($result) {
            $models = array(
                "status" => "1",
                "message" => "",
                "error" => "",
                "info" => $result,
            );
        } else {
            $models = array(
                "status" => "0",
                "message" => "result not found.",
                "error" => "",
                "info" => '',
            );
        }

        return $models;
    }
        
        public function getOrdersHistory($client_id) {

        $result = Yii::app()->db->createCommand()
                ->select('p.*,u.full_name,d.item_name,d.size_price as price,d.quantity as total_purchased_quantity,d.created_at as purchased_date')
                ->from('oreders p')
                ->leftJoin('user u', 'u.id = p.buyer_id')
                ->leftJoin('oreders_details d', 'd.order_id = p.id')
                ->where('buyer_id=:buyer_id',array(':buyer_id'=>$client_id))
                ->queryAll();
        if ($result) {
            $models = array(
                "status" => "1",
                "message" => "",
                "error" => "",
                "info" => $result,
            );
        } else {
            $models = array(
                "status" => "0",
                "message" => "result not found.",
                "error" => "",
                "info" => '',
            );
        }

        return $models;
    }
}
