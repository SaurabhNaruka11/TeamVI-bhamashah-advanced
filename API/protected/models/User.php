<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property integer $is_active
 * @property string $created_date
 * @property string $modified_date
 * @property string $last_logged_in
 */
class User extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, full_name, email, password_hash, auth_key, password_reset_token, created_date, modified_date, last_logged_in', 'required'),
            array('is_active', 'numerical', 'integerOnly' => true),
            array('username, full_name, email, password_hash, auth_key, password_reset_token', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, full_name, email, password_hash, auth_key, password_reset_token, is_active, created_date, modified_date, last_logged_in', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'full_name' => 'Full Name',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'is_active' => 'Is Active',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'last_logged_in' => 'Last Logged In',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('full_name', $this->full_name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password_hash', $this->password_hash, true);
        $criteria->compare('auth_key', $this->auth_key, true);
        $criteria->compare('password_reset_token', $this->password_reset_token, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('modified_date', $this->modified_date, true);
        $criteria->compare('last_logged_in', $this->last_logged_in, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getUsers() {

        $result = Yii::app()->db->createCommand()
                ->select('*')
                ->from('user')
//                ->where('id=:id', array(':id' => $id))
                ->where('is_active = :is_active', array('is_active' => 1))
                ->queryAll();

        if ($result) {
            $models = array(
                "status" => "1",
                "message" => "",
                "error" => "",
                "info" => $result,
            );
        } else {
            $models = array(
                "status" => "0",
                "message" => "result not found.",
                "error" => "",
                "info" => '',
            );
        }

        return $models;
    }

    public function getUserDetails($id) {

        $result = Yii::app()->db->createCommand()
                ->select('u.*,c.is_selling,c.latitude,c.longitude,C.map_address,C.dd_antyodya_id')
                ->from('user u')
                ->leftJoin('user_info c', 'c.user_id = u.id')
                ->where('u.id=:id', array(':id' => $id))
                ->andwhere('is_active = :is_active', array('is_active' => 1))
                ->queryAll();
        if ($result) {
            $models = array(
                "status" => "1",
                "message" => "",
                "error" => "",
                "info" => $result,
            );
        } else {
            $models = array(
                "status" => "0",
                "message" => "result not found.",
                "error" => "",
                "info" => '',
            );
        }

        return $models;
    }

}
