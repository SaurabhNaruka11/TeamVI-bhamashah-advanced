<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property string $product_name
 * @property integer $product_category
 * @property string $product_image
 * @property integer $is_available
 * @property integer $product_owner
 * @property double $price
 * @property string $stock
 * @property string $created_date
 * @property string $modified_date
 */
class Product extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'product';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_name, product_category, product_image, is_available, product_owner, price, stock, created_date, modified_date', 'required'),
            array('product_category, is_available, product_owner', 'numerical', 'integerOnly' => true),
            array('price', 'numerical'),
            array('product_name, product_image, stock', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, product_name, product_category, product_image, is_available, product_owner, price, stock, created_date, modified_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'product_name' => 'Product Name',
            'product_category' => 'Product Category',
            'product_image' => 'Product Image',
            'is_available' => 'Is Available',
            'product_owner' => 'Product Owner',
            'price' => 'Price',
            'stock' => 'Stock',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('product_name', $this->product_name, true);
        $criteria->compare('product_category', $this->product_category);
        $criteria->compare('product_image', $this->product_image, true);
        $criteria->compare('is_available', $this->is_available);
        $criteria->compare('product_owner', $this->product_owner);
        $criteria->compare('price', $this->price);
        $criteria->compare('stock', $this->stock, true);
        $criteria->compare('created_date', $this->created_date, true);
        $criteria->compare('modified_date', $this->modified_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getProducts($client_id) {

        $result = Yii::app()->db->createCommand()
                ->select('p.*,c.category_name as product_category,u.full_name as product_owner,i.latitude,i.longitude,i.map_address')
                ->from('product p')
                ->leftJoin('product_catgory c', 'c.id = p.product_category')
                ->leftJoin('user u', 'u.id = p.product_owner')
                ->leftJoin('user_info i', 'i.user_id = u.id')
                ->where('product_owner!=:product_owner', array(':product_owner' => $client_id))
                ->andwhere('is_available = :is_available', array('is_available' => 1))
                ->queryAll();
        if ($result) {
            $models = array(
                "status" => "1",
                "message" => "",
                "error" => "",
                "info" => $result,
            );
        } else {
            $models = array(
                "status" => "0",
                "message" => "result not found.",
                "error" => "",
                "info" => '',
            );
        }

        return $models;
    }
    
    /*
     * get all the products added by the logeedin user
     */
    public function getUserAddedProducts($client_id) {

        $result = Yii::app()->db->createCommand()
                ->select('p.*,c.category_name as product_category,u.full_name as product_owner,i.latitude,i.longitude,i.map_address')
                ->from('product p')
                ->leftJoin('product_catgory c', 'c.id = p.product_category')
                ->leftJoin('user u', 'u.id = p.product_owner')
                ->leftJoin('user_info i', 'i.user_id = u.id')
                ->where('product_owner=:product_owner', array(':product_owner' => $client_id))
                ->andwhere('is_available = :is_available', array('is_available' => 1))
                ->queryAll();
        if ($result) {
            $models = array(
                "status" => "1",
                "message" => "",
                "error" => "",
                "info" => $result,
            );
        } else {
            $models = array(
                "status" => "0",
                "message" => "result not found.",
                "error" => "",
                "info" => '',
            );
        }

        return $models;
    }

}
