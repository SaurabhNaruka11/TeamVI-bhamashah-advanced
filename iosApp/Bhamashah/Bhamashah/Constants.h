//
//  Constants.h
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#define MainStoryBoard      [UIStoryboard storyboardWithName:@"Main" bundle:nil]
#define kLangualString(msg)       [NSString stringWithFormat:NSLocalizedStringFromTable(msg,[UserDefaultManager getLanguage],nil)]
#endif /* Constants_h */
