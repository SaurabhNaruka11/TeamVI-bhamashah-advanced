//
//  UserDefaultManager.m
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import "UserDefaultManager.h"
#define kLanguage               @"language"

@implementation UserDefaultManager


// LANGUAGE
+ (NSString *)getLanguage {
    NSLog(@"language: %@",[[NSUserDefaults standardUserDefaults] objectForKey:kLanguage]);
    return [[NSUserDefaults standardUserDefaults] objectForKey:kLanguage];
}

+ (void)setLanguage:(NSString *)language {
    if(language.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:language forKey:kLanguage];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLanguage];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
}


@end
