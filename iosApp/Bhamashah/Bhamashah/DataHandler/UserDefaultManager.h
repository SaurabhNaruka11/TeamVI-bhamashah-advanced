//
//  UserDefaultManager.h
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultManager : NSObject

//Language
+ (NSString *)getLanguage;
+ (void)setLanguage:(NSString *)language;

@end
