//
//  AppDelegate.h
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 18/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

