//
//  TutorialViewController.m
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import "TutorialViewController.h"
#import <EAIntroPage.h>
#import <EAIntroView.h>

@interface TutorialViewController ()<EAIntroDelegate>
@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.bgImage = [UIImage imageNamed:@"tutorial1"];
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.bgImage = [UIImage imageNamed:@"tutorial2"];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.bgImage = [UIImage imageNamed:@"tutorial3"];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3]];
    
    [intro setDelegate:self];
    
    [intro showInView:self.view animateDuration:0.0];
    
    [intro.skipButton addTarget:self action:@selector(change) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)navigateToLanguageSelct {
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *home = [MainStoryBoard instantiateViewControllerWithIdentifier:@"languageSelectVC"];
    UINavigationController *nav                    = [[UINavigationController alloc]initWithRootViewController:home];
    nav.navigationBar.barTintColor                 = [UIColor orangeColor];
    nav.navigationBar.titleTextAttributes          = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    app.window.rootViewController = nav;
    [self presentViewController:nav
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)change {
    [self navigateToLanguageSelct];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped {
    [self navigateToLanguageSelct];
}
#pragma mark - Sample Delegate Methods


@end
