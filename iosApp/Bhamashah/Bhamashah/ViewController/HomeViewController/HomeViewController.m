//
//  HomeViewController.m
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet UIButton *enrollDetail;
@property (weak, nonatomic) IBOutlet UIButton *bhamaID;
@property (weak, nonatomic) IBOutlet UIButton *adharID;
@property (weak, nonatomic) IBOutlet UIButton *yojna;
@property (weak, nonatomic) IBOutlet UIButton *dbt;
@property (weak, nonatomic) IBOutlet UIButton *emitra;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBg"]]];
    [_bhamaID setTitle:kLangualString(@"Bhamashah ID") forState:UIControlStateNormal];
    [_enrollDetail setTitle:kLangualString(@"Enrollment Detail") forState:UIControlStateNormal];
    [_adharID setTitle:kLangualString(@"Aadhar ID") forState:UIControlStateNormal];
    [_yojna setTitle:kLangualString(@"Antyodaya") forState:UIControlStateNormal];
    [_dbt setTitle:kLangualString(@"DBT") forState:UIControlStateNormal];
    [_emitra setTitle:kLangualString(@"E-Mitra") forState:UIControlStateNormal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)homeMenuOptions:(UIButton *)sender {
    switch (sender.tag) {
        case 6: {
            UIViewController *vc = [MainStoryBoard instantiateViewControllerWithIdentifier:@"emitraVC"];
            [self.navigationController pushViewController:vc animated:YES];
        }   break;
            
        default:
            break;
    }
}



@end
