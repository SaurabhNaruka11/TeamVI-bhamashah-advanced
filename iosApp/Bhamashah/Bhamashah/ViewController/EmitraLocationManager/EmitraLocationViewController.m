//
//  EmitraLocationViewController.m
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 21/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import "EmitraLocationViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface EmitraLocationViewController ()<CLLocationManagerDelegate>
@property (strong,nonatomic)   GMSMapView          *mapView;
@property (nonatomic, retain) CLLocationManager *locationManager;

@end

@implementation EmitraLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _mapView = [[GMSMapView alloc] init];
    self.view = _mapView;
    
    _mapView.userInteractionEnabled = YES;
    NSDictionary *dict1 = @{@"latitude":@"26.889849",
                            @"lognitude":@"75.832439"
                            };
    
    NSDictionary *dict2 = @{@"latitude":@"26.8662316",
                            @"lognitude":@"75.7836911"
                            };
    
    NSDictionary *dict3 = @{@"latitude":@"26.9047722",
                            @"lognitude":@"75.7990277"
                            };
    NSArray *array = [[NSArray alloc] initWithObjects:dict1,dict2,dict3, nil];
    NSMutableArray *newArray = [NSMutableArray new];
    for (NSDictionary *dict in array) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([dict[@"latitude"] floatValue],[dict[@"lognitude"] floatValue]);
        marker.title = @"E-Mitra";
        [newArray addObject:marker];
        marker.map = _mapView;
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        
        for (GMSMarker *marker in newArray) {
            bounds = [bounds includingCoordinate:marker.position];
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.0f]];
    }
    [_mapView animateToZoom:13];
   
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationItem.title = kLangualString(@"E-mitra Location");
    self.addLeftBarBarBackButtonEnabled = YES;
}

@end

