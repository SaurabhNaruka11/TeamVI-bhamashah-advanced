//
//  BhamashahViewController.m
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import "BhamashahViewController.h"

@interface BhamashahViewController ()

@end

@implementation BhamashahViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setAddLeftBarBarBackButtonEnabled:(BOOL)addLeftBarBarBackButtonEnabled {
    //This is for add back button and it should be called from viewWillAppear
    if (addLeftBarBarBackButtonEnabled) {
        UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnBack setFrame:CGRectMake(0, 0, 20, 20)];
        [btnBack setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
        [btnBack addTarget:self action:@selector(actionLeftBarButton:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButton =[[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = barButton;
    } else {
        [self.navigationItem setHidesBackButton:YES];
    }
}

- (void)actionLeftBarButton:(UIButton*)sender {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [topController.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
