//
//  LanguageViewController.m
//  Bhamashah
//
//  Created by Roshan Singh Bisht on 20/03/17.
//  Copyright © 2017 Roshan Singh Bisht. All rights reserved.
//

#import "LanguageViewController.h"

@interface LanguageViewController ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UITextField *textLanguage;
@property (strong,nonatomic)NSArray         *languageArray;
@property (weak, nonatomic) IBOutlet UIButton *disableButton;
@property (nonatomic, strong) UIToolbar *toolbar;
@end

@implementation LanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _languageArray = [[NSArray alloc] initWithObjects:@"English",@"हिंदी",@"मारवाड़ी", nil];
    UIPickerView *picker = [[UIPickerView alloc] init];
    picker.dataSource = self;
    picker.delegate = self;
    _toolbar = [[UIToolbar alloc] init];
    _textLanguage.textAlignment = NSTextAlignmentCenter;
    [_toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [_toolbar sizeToFit];
    UIBarButtonItem  *buttonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Done", @"BSKeyboardControls", @"Done button title.")
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(doneBtnPressToGetValue)];
    buttonItem.tintColor = [UIColor whiteColor];
    NSArray *itemsArray = [NSArray arrayWithObjects:buttonItem, nil];
    [_toolbar setItems:itemsArray];
    [_textLanguage setInputAccessoryView:_toolbar];
    [_textLanguage setInputView:picker];
    _continueButton.layer.borderWidth = 1.0;
    _continueButton.layer.cornerRadius = 5.0;
    _continueButton.clipsToBounds = YES;
}

- (void)doneBtnPressToGetValue {
    [self.view endEditing:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    _textLanguage.text = [_languageArray objectAtIndex:row];
    return [_languageArray objectAtIndex:row];
}
- (IBAction)continueAction:(id)sender {
    if ([_textLanguage.text isEqualToString:@"English"]) {
        [UserDefaultManager setLanguage:@"English"];
    } else if ([_textLanguage.text isEqualToString:@"हिंदी"]) {
        [UserDefaultManager setLanguage:@"Localizable"];
    } else {
        [UserDefaultManager setLanguage:@"Marwai"];
    }
    if (_textLanguage.text.length > 0) {
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        UIViewController *home = [MainStoryBoard instantiateViewControllerWithIdentifier:@"homeVC"];
        UINavigationController *nav                    = [[UINavigationController alloc]initWithRootViewController:home];
        nav.navigationBar.barTintColor                 = [UIColor orangeColor];
        nav.navigationBar.titleTextAttributes          = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
        app.window.rootViewController = nav;
        [self presentViewController:nav
                           animated:YES
                         completion:^{
                             
                         }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please choose your language" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}

- (UIAccessibilityTraits)accessibilityTraits
{
    return UIAccessibilityTraitButton;
}

- (NSString *)accessibilityHint
{
    return NSLocalizedString(@"MyCustomView.hint", nil);
}
- (IBAction)visualAction:(id)sender {
    NSArray* urlStrings = @[@"prefs:root=General&path=ACCESSIBILITY", @"App-Prefs:General&path=ACCESSIBILITY"];
    for(NSString* urlString in urlStrings){
        NSURL* url = [NSURL URLWithString:urlString];
        if([[UIApplication sharedApplication] canOpenURL:url]){
            [[UIApplication sharedApplication] openURL:url];
            break;
        }
    }

}
@end
